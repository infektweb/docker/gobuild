FROM golang:1.19

RUN for i in gnupg ssh; do mkdir -m 700 -p ~/.$i/; done

ADD deployment/bin/*.sh /usr/bin/
